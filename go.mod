module gitee.com/jkuang/sm2-eth

go 1.13

require (
	gitee.com/jkuang/go-fastecdsa v0.6.3
	github.com/cespare/cp v0.1.0
	github.com/davecgh/go-spew v1.1.1
	github.com/deckarep/golang-set v0.0.0-20180603214616-504e848d77ea
	github.com/ethereum/go-ethereum v1.10.0
	github.com/google/uuid v1.1.5
	github.com/rjeczalik/notify v0.9.1
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
)
